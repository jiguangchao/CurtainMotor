
#ifndef _BASELIB_INCLUDE_H_
#define _BASELIB_INCLUDE_H_

#include "platform.h"
#include "..\MCU\BSP\BSP_Include.h"
#include ".\CRC\CRC_8_16_32.h"

#include ".\smSerial\smSerial.h"
#include ".\VCP\VCP_IIC.h"
#include ".\VCP\VCP_SPI.h"
#include ".\Build\BuildVersion.h"
#include ".\DataConvert\DataConvert.h"
#include ".\TM1617\TM1617.h"
#include ".\M24CXX\M24CXX.h"



#endif
